let http = require("http");
let url = require("url");
let router = require("routes")();
let view = require("swig");

router.addRoute('/', (req,res)=>{
    let html = view.compileFile('./template/index.html')({
        title : "Index Page from Dandi Indra Wijaya"
    });
    res.writeHead(200, {"Content-Type" : "text/html"});
    res.end(html);
})

router.addRoute('/contact', (req,res)=>{
    let html = view.compileFile('./template/contact.html')();
    res.writeHead(200, {"Content-Type" : "text/html"});
    res.end(html);
})

http.createServer((req, res) => {
    let path = url.parse(req.url).pathname;
    let match = router.match(path);

    if(match){
        match.fn(req, res);
    }else{
        res.writeHead(404, {"Content-Type" : "text/plain"});
        res.end("Page not Found");
    }
    
}).listen(8888);

console.log("Server is running");