let http = require("http");
let url = require("url");
let routes = require("routes")();

routes.addRoute('/', function(req, res){
    res.writeHead(200, {"Content-Type" : "text/plain"});
    res.end("Index Page");
})

routes.addRoute('/profile/:nama/:kota?', function(req, res){
    res.writeHead(200, {"Content-Type" : "text/plain"});
    res.end("Profile Page ==> " + this.params.nama + " dari kota "+ this.params.kota);
})

http.createServer((req, res) => {
    let path = url.parse(req.url).pathname;
    let match = routes.match(path);
    if(match){
        match.fn(req, res);
    }else{
        res.writeHead(404, {"Content-Type" : "text/plain"});
        res.end("Page Not Found !!!");
    }
      
}).listen(8080);

console.log("Server is running....");