let http = require("http");
let fs = require('fs');

http.createServer((req, res) => {
    let kode = 200;
    let file = "";

    if(req.url == "/"){
        //index
        kode = 200;
        file = 'index.html';

    }else if(req.url == '/contact'){
        //contact
        kode = 200;
        file = 'contact.html';
    }else{
        // 404 /not found
        kode = 404;
        file = "404.html";
    }
    res.writeHead(kode, {"Content-Type" : "text/html"});
    fs.createReadStream('./template/' + file).pipe(res);
    
}).listen(8888);

console.log("Server is running....");