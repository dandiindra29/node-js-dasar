let http = require("http");
let url = require("url");
let router = require("routes")();
let view = require("swig");
let mysql = require("mysql");

let connection = mysql.createConnection({
    host : "localhost",
    port: "3306",
    database: "nodejs",
    user : "root",
    password: ""
});

router.addRoute('/', (req,res)=>{
    connection.query("SELECT * FROM mahasiswa", (err, rows, filed) =>{
        if(err) throw err;
        
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end(JSON.stringify(rows));
    })
})

router.addRoute('/insert', (req, res) => {

    connection.query("INSERT INTO mahasiswa set ?", {
        nomor_induk : "4611418016",
        nama: "Oktaviani",
        alamat: "Purbalingga"
    }, function(err, field){
        if(err) throw err;

        res.writeHead(200, {"Content-Type" : "text/plain"})
        res.end(field.affectedRows + " Affected Rows");
    });
})

router.addRoute('/update', (req, res) => {
    
    connection.query("UPDATE mahasiswa set ? where ?",[
        { nama: "Oktaviani Sinta Dewi"},
        { nomor_induk: "4611418016" }
    ], (err, field) => {
        if(err) throw err;

        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end(field.changeRows + " Rows Updated");
    })
})

router.addRoute('/delete', (req, res) => {
    
    connection.query("DELETE FROM mahasiswa where ?", {
        nomor_induk : "4611418016"
    }, (err, field) => {
        if(err) throw err;
        
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end(field.affectedRows + " Rows Deleted")
    })
})

router.addRoute('/contact', (req,res)=>{
    let html = view.compileFile('./template/contact.html')();
    res.writeHead(200, {"Content-Type" : "text/html"});
    res.end(html);
})

http.createServer((req, res) => {
    let path = url.parse(req.url).pathname;
    let match = router.match(path);

    if(match){
        match.fn(req, res);
    }else{
        res.writeHead(404, {"Content-Type" : "text/plain"});
        res.end("Page not Found");
    }
    
}).listen(8888);

console.log("Server is running");